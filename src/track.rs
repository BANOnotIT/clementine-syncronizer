use rusqlite::{Connection, Error};
use std::fmt;
use std::path::PathBuf;
use url::percent_encoding::percent_decode;
use url::Url;


pub type TrackID = u32;
type Blob = Vec<u8>;


pub struct Track {
    pub id: TrackID,
    pub title: String,
    pub album: String,
    pub artist: String,
    pub filename: PathBuf,
}

impl fmt::Display for Track {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} - {}", self.title, self.artist)
    }
}

impl fmt::Debug for Track {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Track {{ {} - {} }}[ {:?} ]", self.title, self.artist, self.filename)
    }
}

impl Track {
    pub fn from_db(conn: &Connection, id: TrackID) -> Result<Track, Error> {
        let track_sql = "
        SELECT
            title,
            album,
            artist,
            filename
        FROM
            songs
        WHERE
            ROWID = ?
        ";

        let result = (conn).query_row(track_sql, &[&id], |row| {
            Track::new(
                id,
                row.get(0),
                row.get(1),
                row.get(2),
                row.get(3),
            )
        });

        Ok(result?)
    }
    pub fn new(id: u32, title: String, album: String, artist: String, filename: Option<Blob>) -> Track {
        let filename = Track::get_filepath(
            String::from_utf8(filename.unwrap())
                .unwrap()
        );
        Track {
            id,
            title,
            album,
            artist,
            filename,
        }
    }


    fn get_filepath(path: String) -> PathBuf {
        let url = Url::parse(&path)
            .unwrap();

        let path = percent_decode(url.path().as_ref())
            .decode_utf8()
            .unwrap()
            .to_string();

        PathBuf::from(path)
    }

    pub fn filename_from_template(&self, template: &String) -> String {
        let name = template
            .replace("%artist", &self.artist)
            .replace("%title", &self.title)
            .replace("%album", &self.album)
//            removing unsafe charters
            .replace('/', "_")
            .replace('\\', "_")
            .replace(':', "_")
            .replace('\"', "_")
            .replace('*', "_")
            .replace('?', "_")
            .replace('<', "_")
            .replace('>', "_")
            .replace('|', "_")
        ;

        format!(
            "{}.{:x}.{}",
            name,
            &self.id,
            self.filename.to_path_buf().extension().unwrap().to_str().unwrap()
        )
    }
}
