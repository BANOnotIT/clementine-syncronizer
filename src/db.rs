use rusqlite::{Connection, Result};
use std::path::PathBuf;
use track::TrackID;

pub fn open(path: PathBuf) -> Result<DbConnection> {
    Connection::open(path)
        .map(|conn| {
            DbConnection { conn }
        })
}

pub struct DbConnection { pub conn: Connection }

impl DbConnection {
    fn get_playlist_id(&self, playlist_name: String) -> Result<u32> {
        let playlist_id_sql = "\
                SELECT ROWID
                FROM \"playlists\"\
                WHERE name = ?
        ";


        let res = self.conn.query_row(
            playlist_id_sql,
            &[&playlist_name],
            |row| {
                let i: u32 = row.get(0);
                i
            },
        );

        res
    }

    pub fn fetch_track_ids_from_playlist(&self, playlist: String) -> Vec<TrackID> {
        let playlist_id = self.get_playlist_id(playlist).unwrap();

        let track_sql = "
        SELECT
            library_id
        FROM
            playlist_items
        WHERE
            library_id not NULL
            AND
            playlist = ?
        ";

        let mut stmt =
            self.conn.prepare(track_sql)
                .unwrap();

        return stmt
            .query_map(&[&playlist_id], |row| {
                row.get(0)
            })
            .expect("Failed to fetch rows")
            .map(|a| a.unwrap())
            .collect();
    }
}