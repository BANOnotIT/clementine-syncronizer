use std::io::{self, Write};

use terminal_size::{terminal_size, Width};

/// Struct that used for presenting progress bar with plain texts.
///
/// It looks like:
///
/// ```shell
/// Doing something            [===-------] 70%
/// ```
///
/// # Examples
///
/// ```
/// use std::thread;
///
/// extern crate progress;
///
/// fn main() {
///     let bar = progress::Bar::new();
///
///     bar.set_job_title("Working...");
///
///     for i in 0..11 {
///         thread::sleep_ms(100);
///         bar.reach_percent(i * 10);
///     }
/// }
pub struct Bar {
    _job_title: String,
    _progress_percentage: i32,
    _left_cap: String,
    _right_cap: String,
    _filled_symbol: String,
    _circle_symbols: Vec<char>,
    _finished_symbol: char,
    _tick_count: usize,
    _empty_symbol: String,
}

impl Bar {
    /// Create a new progress bar.
    pub fn new() -> Bar {
        Bar {
            _job_title: String::new(),
            _progress_percentage: 0,
            _left_cap: String::from("["),
            _right_cap: String::from("]"),
            _filled_symbol: String::from("="),
            _empty_symbol: String::from("-"),
            _tick_count: 0,
            _circle_symbols: vec!['\\', '|', '/', '-'],
            _finished_symbol: '*',
        }
    }

    /// Reset progress percentage to zero and job title to empty string. Also
    /// prints "\n".
    pub fn jobs_done(&mut self) {
        self._job_title.clear();
        self._progress_percentage = 0;
        self._tick_count = 0;

        print!("\n");
    }

    /// Set text shown in progress bar.
    pub fn set_job_title(&mut self, new_title: &str) {
        self._job_title.clear();
        self._job_title.push_str(new_title);
        if let Some((idx, _)) = self._job_title.char_indices().nth(50) {
            self._job_title.truncate(idx)
        }

        self._show_progress();
    }

    /// Put progress to given percentage.
    pub fn reach_percent(&mut self, percent: i32) {
        self._progress_percentage = percent;
        self._show_progress();
        self.tick();
    }

    pub fn tick(&mut self) {
        self._tick_count += 1;
    }

    /// Increase progress with given percentage.
    pub fn add_percent(&mut self, progress: i32) {
        self._progress_percentage += progress;
        self._show_progress();
        self.tick();
    }
}

impl Bar {
    pub fn get_terminal_size(&self) -> i32 {
        if let Some((Width(w), _)) = terminal_size() {
            w as i32
        } else {
            81 as i32
        }
    }

    fn _show_progress(&self) {
        let width = self.get_terminal_size();
        let overhead = self._progress_percentage / 100;
        let left_percentage = self._progress_percentage - overhead * 100;
        let bar_len = width - (50 + 5) - 2 - 5;
        let bar_finished_len = ((bar_len as f32) *
            (left_percentage as f32 / 100.0)) as i32;
        let filled_symbol = if overhead & 0b1 == 0 {
            &self._filled_symbol
        } else {
            &self._empty_symbol
        };
        let empty_symbol = if overhead & 0b1 == 0 {
            &self._empty_symbol
        } else {
            &self._filled_symbol
        };

        io::stdout().flush().unwrap();
        print!("\r");

        print!("{}", self._left_cap);
        print!("{}", self._show_cirle());
        print!("{}", self._right_cap);
        print!(" {:<50} ", self._job_title);
        print!("{}", self._left_cap);
        for _ in 0..bar_finished_len {
            print!("{}", filled_symbol);
        }
        for _ in bar_finished_len..bar_len {
            print!("{}", empty_symbol);
        }
        print!("{}", self._right_cap);
        print!("{:>4}%", self._progress_percentage);
    }

    fn _show_cirle(&self) -> &char {
        self._circle_symbols.get(
            self._tick_count % self._circle_symbols.len()).unwrap()
    }
}
